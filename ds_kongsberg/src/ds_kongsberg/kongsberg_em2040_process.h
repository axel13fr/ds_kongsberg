#ifndef KONGSBERG_EM2040_PROCESS_H
#define KONGSBERG_EM2040_PROCESS_H

#include <ds_base/ds_process.h>
#include <lib_kongsberg_em/kongsberg_em2040.h>

namespace ds_kongsberg{

struct KongsbergEM2040ProcessPrivate{
  KongsbergEM2040ProcessPrivate(ros::NodeHandle nh,kongsberg_em::SendKCtrlDataFunc f)
      : node_(nh,f)
  {}

  kongsberg_em::KongsbergEM2040 node_;

  // UDP connection to data stream
  boost::shared_ptr<ds_asio::DsConnection> kmall_conn_ = nullptr;

  // UDP connection to kctrl for sending/receiving commands
  boost::shared_ptr<ds_asio::DsConnection> kctrl_conn_ = nullptr;
};

class KongsbergEM2040Process : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(KongsbergEM2040Process)
  DS_DISABLE_COPY(KongsbergEM2040Process)
 public:
  KongsbergEM2040Process(int argc, char *argv[], const std::string &name);
  ~KongsbergEM2040Process() override;

 protected:
  void setupServices() override;
  void setupPublishers() override;
  void setupParameters() override;
  void setupConnections() override;
  void setupTimers() override;

 private:
  void _on_kmall_data(ds_core_msgs::RawData raw);
  void _on_kctrl_data(ds_core_msgs::RawData raw);
  void _send_ktrl_data(const std::string &data);

  std::unique_ptr<KongsbergEM2040ProcessPrivate> d_ptr_;
};
}
#endif // KONGSBERG_EM2040_PROCESS_H
