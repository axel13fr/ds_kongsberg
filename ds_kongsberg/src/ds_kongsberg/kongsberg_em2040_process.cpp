#include "kongsberg_em2040_process.h"
#include <memory>
using namespace ds_kongsberg;
using namespace kongsberg_em;

KongsbergEM2040Process::KongsbergEM2040Process(int argc, char *argv[], const std::string &name)
    : ds_base::DsProcess(argc, argv, name),
      d_ptr_(new KongsbergEM2040ProcessPrivate(nodeHandle(),
             // Callback given to the ROS node to send data using the kctrl_connection
             std::bind(&KongsbergEM2040Process::_send_ktrl_data, this,std::placeholders::_1)))
{
}

KongsbergEM2040Process::~KongsbergEM2040Process()
{
}

void KongsbergEM2040Process::setupServices()
{
  ds_base::DsProcess::setupServices();
  d_ptr_->node_.setupServices();
}

void KongsbergEM2040Process::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
  d_ptr_->node_.setupPublishers();
}

void KongsbergEM2040Process::setupParameters()
{
  ds_base::DsProcess::setupParameters();
  d_ptr_->node_.setupParameters();
}

void KongsbergEM2040Process::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  DS_D(KongsbergEM2040Process);
  d->kmall_conn_ = addConnection("kmall_connection", boost::bind(&KongsbergEM2040Process::_on_kmall_data, this, _1));
  d->kctrl_conn_ = addConnection("kctrl_connection", boost::bind(&KongsbergEM2040Process::_on_kctrl_data, this, _1));
}

void KongsbergEM2040Process::_send_ktrl_data(const std::string &data)
{
  DS_D(KongsbergEM2040Process);
  if (d->kctrl_conn_)
  {
    d->kctrl_conn_->send(data);
  }
}

void KongsbergEM2040Process::setupTimers()
{
  ds_base::DsProcess::setupTimers();
  d_ptr_->node_.setupTimers();
}

void KongsbergEM2040Process::_on_kmall_data(ds_core_msgs::RawData raw)
{
  DS_D(KongsbergEM2040Process);
  d->node_._on_kmall_data(raw);
}

void KongsbergEM2040Process::_on_kctrl_data(ds_core_msgs::RawData raw)
{
  DS_D(KongsbergEM2040Process);
  d->node_._on_kctrl_data(raw);
}
