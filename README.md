# ds_kongsberg

This is the repository for the ros node controlling the Kongsberg EM2040 multibeam and associated files.

## catkin packages

- ds_kongsberg
- ds_kongsberg_msgs
- kongsberg_em_msgs